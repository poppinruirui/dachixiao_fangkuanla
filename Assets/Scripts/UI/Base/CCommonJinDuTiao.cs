﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCommonJinDuTiao : MonoBehaviour {

    public Text _txtInfo;
    public Text _txtJinDu;
    public Image _imgJinDu;

    float m_fCurPercent = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
      

    }

    public void SetCurPercent( float fPercent )
    {
        m_fCurPercent = fPercent;
        if (_txtJinDu != null)
        {
            _txtJinDu.text = fPercent.ToString("f0") + "%";
        }
        _imgJinDu.fillAmount = m_fCurPercent / 100f;
    }

    public void SetInfo( string szInfo )
    {
        if (_txtInfo == null)
        {
            return;
        }
        _txtInfo.text = szInfo;
    }
}
