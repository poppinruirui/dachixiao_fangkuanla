﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCyberTreePoPo : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public Image _imgLeft;
    public Image _imgCenter;
    public Image _imgRight;
    public Text _txtContent;

    public float m_fUnitWidth = 61f;
    public float m_fFontSize = 24f;

    bool m_bActive = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetContent( string szContent )
    {
        _txtContent.text = szContent;
        int nNumOfWords = _txtContent.text.Length;

        byte[] bytes = StringManager.String2Bytes(_txtContent.text);

        float fWidthOfContent = m_fFontSize * nNumOfWords - m_fUnitWidth;
        float fScaleOfCenter = fWidthOfContent / m_fUnitWidth;
        vecTempPos.x = ( m_fUnitWidth + fWidthOfContent ) / 2f;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        _imgCenter.transform.localPosition = vecTempPos;
        vecTempScale.x = fScaleOfCenter;
        vecTempScale.y = 1;
        vecTempScale.z = 1;
        _imgCenter.transform.localScale = vecTempScale;
        vecTempPos.x = m_fUnitWidth + fWidthOfContent;
        vecTempPos.y = 0;
        vecTempPos.z = 0;
        _imgRight.transform.localPosition = vecTempPos;
    }

    public void SetActive( bool bActive )
    {
        m_bActive = bActive;
        this.gameObject.SetActive(bActive);
    }
}

