﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CGesture : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public bool m_bUI = true;

    public SpriteRenderer m_sprMain;
    public Image m_imgMain;
    public Button m_btnMain;
    
    int m_nId = 0;

    // Use this for initialization
    void Start () {
      

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetRaycastEnable( bool bEnable )
    {
        if (m_bUI)
        {
            m_btnMain.enabled = bEnable;
            m_imgMain.raycastTarget = bEnable;
        }
    }

    public void SetSprite( Sprite spr )
    {
        if (m_bUI)
        {
            m_imgMain.sprite = spr;
        }
        else
        {
            m_sprMain.sprite = spr;
        }
    }

    public void SetScale( float scale )
    {
        vecTempScale.x = scale;
        vecTempScale.y = scale;
        vecTempScale.z = 1;
        this.transform.localScale = vecTempScale;
    }

    public int GetID()
    {
        return m_nId;
    }

    public void SetID( int nId )
    {
        m_nId = nId;

        SetSprite( CGestureManager.s_Instance.GetGestureSpriteById(m_nId));
    }


    public void SetLocalPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void OnClickButton_AddMeToSelectedList()
    {
        CGestureManager.s_Instance.AddGestureToSelectedList( this.GetID() );
    }
}
