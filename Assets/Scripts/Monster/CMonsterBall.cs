﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterBall : CPveObj {

    public Ball _ball;

    void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDead( bool val )
    {
        this.gameObject.SetActive( !val );
        _ball.SetDead( val );
    }

    public void SetSize( float val )
    {
        _ball.SetSize( val );
    }

	public float GetRadius()
	{
		return _ball.GetRadius ();
	}
}
