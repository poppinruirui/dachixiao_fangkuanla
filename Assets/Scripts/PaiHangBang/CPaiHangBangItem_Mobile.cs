﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaiHangBangItem_Mobile : MonoBehaviour {

    public Text _txtPlayerName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public void SetPlayerName( string szPlayerName )
    {
        _txtPlayerName.text = szPlayerName;
    }
}

