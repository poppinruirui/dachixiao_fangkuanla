﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaiHangBangItem : MonoBehaviour {

    public Text _txtName;
    public Text _txtNo;
    public Text _txtLevel;
    public Text _txtTotalVolume;
    public Text _txtExplodeThornNum;
    public Text _txtJiShaInfo;
    public Text[] _aryItemNum;

    
    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetName( string szName )
    {
        _txtName.text = szName;
    }

    public void SetLevel( int nLevel )
    {
        _txtLevel.text = "LV." + nLevel;
    }

    public void SetTotalVolume( string szTotalVolume )
    {
        _txtTotalVolume.text = "总体积：" + szTotalVolume;
    }
    
    public void SetNo( string szNo )
    {
        _txtNo.text = szNo;
    }

    public void SetJiShaInfo(int nKillNum, int nBeKilledNum, int nAssistNum)
    {
        _txtJiShaInfo.text = nKillNum + " / " + nBeKilledNum + " / " + nAssistNum;
    }

    public void SetEatThornCount( int nEatThornNum )
    {
        _txtExplodeThornNum.text = nEatThornNum.ToString();
    }

    public void SetItemNum( int nItem0Num, int nItem1Num, int nItem2Num, int nItem3Num )
    {
        _aryItemNum[0].text = nItem0Num.ToString();
        _aryItemNum[1].text = nItem1Num.ToString();
        _aryItemNum[2].text = nItem2Num.ToString();
        _aryItemNum[3].text = nItem3Num.ToString();
    }

}
