﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLight : MonoBehaviour {

    CLightSystem.eLightType m_eType = CLightSystem.eLightType.point;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetType( CLightSystem.eLightType type )
    {
        m_eType = type;
    }

    public CLightSystem.eLightType GetType()
    {
        return m_eType;
    }
}
