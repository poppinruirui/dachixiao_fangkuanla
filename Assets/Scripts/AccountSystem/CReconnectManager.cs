﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CReconnectManager  : Photon.PunBehaviour { 

	public static CReconnectManager s_Instance = null;

	void Awake()
	{
		s_Instance = this;
	}


	// Use this for initialization
	void Start () {
		Reconnect ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Reconnect()
	{


		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectToRegion (CloudRegionCode.cn, AccountManager._gameVersion);
		} else {
			OnConnectedToMaster ();
		
		}
	}

	void OnConnectedToMaster()
	{
		PhotonNetwork.JoinOrCreateRoom(AccountManager.m_szCurSelectedRoomName/*_lstRoomList.GeCurSelectedItemContent()*/, new RoomOptions() { MaxPlayers = 20 }, null);
		PhotonNetwork.playerName = AccountManager.GetPlayerName();
	}

	public void OnClickButton_Reconnect()
	{
		
	}

}
