﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Net;
using UnityEngine.SceneManagement;
//using OpenWinForm = System.Windows.Forms;
public class PolygonEditor : MonoBehaviour {

    public GameObject m_prePolygonPoint;
    public GameObject m_prePolygonLine;
    public GameObject m_prePolygon;
    Vector3 vecTempPos = new Vector3();

    public PolygonPoint m_CurSelectedPoint = null;


    public GameObject the_polygon;
    public Polygon _polygon;
    public PolygonCollider2D _polygonCollider;

    public Button _btnBeginOrEndDrawPath;
    static int s_nPointGUID = 0;

	List<string> m_lstPolygons = new List<string>();
	public ComoList _PolygonList;

	public MsgBox m_MsgBox;

	public CreateNewShit _panelCreateNewPolygon;

	XmlNode _root = null;
	XmlDocument _XmlDoc;

    // Use this for initialization
	void Start () {
      
		_PolygonList.delegateMethodOnSelect = new ComoList.DelegateMethod_OnSelected ( this.OnSelected );
		_panelCreateNewPolygon.delegateMethodOK = new CreateNewShit.DelegateMethod_OK ( this.CreateNewPolygonOK );
		m_MsgBox.delegateMethodOnClickButton = new MsgBox.DelegateMethod_OnClickButton ( this.DoDeletePolygon );	
	
		StartCoroutine ( LoadPolygonList() );
    }

	IEnumerator LoadPolygonList()
	{
		string szFileName =  AccountManager.url + "PolygonList.xml";

		WWW www = new WWW ( szFileName );
		yield return www;
		_XmlDoc = StringManager.CreateXmlByText( www.text, ref _root );
		m_lstPolygons.Clear ();
		for (int i = 0; i < _root.ChildNodes.Count; i++) {
			m_lstPolygons.Add ( _root.ChildNodes[i].InnerText );
		}
		RefreshPolygonList ();
	}

	string m_szCurSelectedPolygonName = "";
	public void OnSelected( int nIndex )
	{
		m_szCurSelectedPolygonName = _PolygonList.GetCurSelectedItem ().GetContent ();
		if (m_szCurSelectedPolygonName.Length != 0) {
			LoadPolygon ( m_szCurSelectedPolygonName );
		}
	}

	void RefreshPolygonList()
	{
		_PolygonList.ClearAll ();
		for (int i = 0; i < m_lstPolygons.Count; i++) {
			_PolygonList.AddItem ( m_lstPolygons[i] );
		}
		if (m_lstPolygons.Count > 0) {
			_PolygonList.SelectByIndex (0);
		}
	}

	// Update is called once per frame
	void Update () {
        ProcessMouseInput();
    }

    void Reset()
    {
        m_dicLine.Clear();
        m_dicLineTemp.Clear();
        m_eOp = ePolygonEditOperation.none;
        if (_polygon)
        {
            _polygon.Reset();
            GameObject.Destroy(_polygon.gameObject);
        }
        ClearCurSelectPoint();
    }

    public static  PolygonEditor s_Instance;
    void Awake()
    {
       
        s_Instance = this;

        Reset();
    }

    public Dictionary<string, PolygonLine> m_dicLine = new Dictionary<string, PolygonLine>();
    public Dictionary<string, PolygonLine> m_dicLineTemp = new Dictionary<string, PolygonLine>();

    PolygonPoint m_CurMovingPoint = null;

    public enum ePolygonEditOperation
    {
        none,
        draw_path,
        add_point,
        remove_point,
    };

    ePolygonEditOperation m_eOp = ePolygonEditOperation.none;


    // 判断当前是否点击在了UI上
    public bool IsPointerOverUI()
    {

        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

	public void CreateNewPolygon()
	{
		_panelCreateNewPolygon.gameObject.SetActive ( true );
	}

    public void BeginOrEndDrawPath()
    {
       //if (m_eOp == ePolygonEditOperation.none)
       // {
            BeginDrawPath();
       // }
       /*
       else if ( m_eOp == ePolygonEditOperation.draw_path )
        {
            EndDrawPath();
        }
       */
    }

	public void DeletePolygon()
	{
		if ( _PolygonList.GeCurSelectedItemContent ().Length == 0) {
			m_MsgBox.ShowMsg ("都木有选中多边形啊", global::MsgBox.eMsgBoxType.e_Msgbox_Type_OkOnly);
			return;
		}

		string szContent = "即将删除的多边形是 【" + _PolygonList.GeCurSelectedItemContent () + "】\n";
		szContent += "将直接删除服务器端存档文件，请谨慎操作。";
		m_MsgBox.ShowMsg (szContent, global::MsgBox.eMsgBoxType.e_Msgbox_Type_YesNo, 1);
	}

	public void DoDeletePolygon( int nParam )
	{
		bool bFound = false;
		for (int i = 0; i < m_lstPolygons.Count; i++) {
			if (m_szCurSelectedPolygonName == m_lstPolygons [i]) {
				m_lstPolygons.RemoveAt (i);
				bFound = true;
				break;
			}
		}

		if (!bFound) {
			Debug.LogError ( "!bFound" );
			return;
		}

		if (_polygon) {
			if (_polygon)
			{
				ResourceManager.DestroyPolygon( _polygon );
			}
		}
		RefreshPolygonList ();

		UpdatePolygonListXmlFile ();
	}

	public void WannaRedraw()
	{
		BeginDrawPath ();
	}

    void BeginDrawPath()
    {
        if (_polygon)
        {
			ResourceManager.DestroyPolygon( _polygon );
        }
        Reset();
        _polygon = NewPolygon();
        _polygon.transform.localPosition = Vector3.zero;
        m_eOp = ePolygonEditOperation.draw_path;
    }

    public void MsgBox( string szMsg )
    {
        Debug.Log(szMsg );
    }

    void EndDrawPath()
    {
        if ( _polygon.m_lstPoint.Count < 2 )
        {
            MsgBox("至少要两个顶点（一条线段）");
            return;
        }

       // _polygon.Link2PointsToGenerateLine( _polygon.m_lstPoint[_polygon.m_lstPoint.Count - 1], _polygon.m_lstPoint[0]);
      
        m_eOp = ePolygonEditOperation.none;
    }

    public void AddOnePoint()
    {
        m_eOp = ePolygonEditOperation.add_point;
        ClearCurSelectPoint();
    }

    public void RemoveOnePoint()
    {
        m_eOp = ePolygonEditOperation.remove_point;
        ClearCurSelectPoint();
    }

    void ProcessMouseInput()
    {
            if (IsPointerOverUI())
            {
                return;
            }

            if (Input.GetMouseButton(0)) // 鼠标左键长按
            {
                MoveOnePoint();
            }
            else
            {
               
            }

            if (Input.GetMouseButtonDown(0)) // 鼠标左键点击
            {
                if ( m_eOp == ePolygonEditOperation.draw_path )
                {
                LayDownOnePoint();
                }
            }

            if (Input.GetMouseButtonUp(0)) // 鼠标左键弹起
            {
              m_CurMovingPoint = null;
            }

            if (Input.GetMouseButtonDown(1))
            {
            if (m_eOp == ePolygonEditOperation.draw_path)
            {
                EndDrawPath();
            }
        }
     }



    void OnLineChange()
    {
        m_dicLineTemp.Clear();
        foreach (KeyValuePair<string, PolygonLine> pair in m_dicLine)
        {
            PolygonLine line = pair.Value;
            string key = line.UpdateMyKey();
            m_dicLineTemp[key] = line;
        }

        m_dicLine.Clear();
        foreach (KeyValuePair<string, PolygonLine> pair in m_dicLineTemp)
        {
            m_dicLine[pair.Key] = pair.Value;
        }
    }

    void LayDownOnePoint()
    {
        int nPointN = _polygon.m_lstPoint.Count;
        PolygonPoint point = NewPoint();
        point.gameObject.transform.parent = _polygon.transform;
        point.gameObject.transform.localPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _polygon.m_lstPoint.Add(point);
        _polygon.OnPointListChange();
        //point.SetIndex(nPointN);

        if (nPointN >= 1)
        {
            int nPrevIndex = nPointN - 1;
            PolygonPoint prev_point = _polygon.m_lstPoint[nPrevIndex];
            _polygon.Link2PointsToGenerateLine(point, prev_point);
        }
    }

    public void PickOnePoint( PolygonPoint point )
    {
        m_CurMovingPoint = point;

        if ( m_eOp == ePolygonEditOperation.add_point )
        {
            PickOnePoint_AddOnePoint(point);
          
        }
        else if (m_eOp == ePolygonEditOperation.remove_point)
        {
            PickOnePoint_RemoveOnePoint(point);
        }


    }

    void PickOnePoint_AddOnePoint(PolygonPoint point)
    {
        if ( m_CurSelectedPoint == null )
        {
            m_CurSelectedPoint = point;
            point.SetColor( Color.yellow );
            return;
        }

        if (m_CurSelectedPoint == point)
        {
            // 选择同一个顶点等于取消选择
            ClearCurSelectPoint();
            return;
        }

        int nDelta = m_CurSelectedPoint.GetIndex() - point.GetIndex();
        if (nDelta != 1 && nDelta != -1 && nDelta != ( _polygon.m_lstPoint.Count - 1 )  && nDelta != ( 1 - _polygon.m_lstPoint.Count ))
        {
            MsgBox("必须选择两个相邻的顶点！");
            Debug.Log(m_CurSelectedPoint.GetIndex() + " , " + point.GetIndex());
            return;
        }

        PolygonPoint p1 = m_CurSelectedPoint;
        PolygonPoint p2 = point;
        int nIndex1 = p1.GetIndex();
        int nIndex2 = p2.GetIndex();
        int nOtherIndex = 0;
        int nNewIndex = 0;
        if ( nIndex1 > nIndex2 )
        {
            nNewIndex = nIndex1;
            nOtherIndex = nIndex2;
        }
        else
        {
            nNewIndex = nIndex2;
            nOtherIndex = nIndex1;
        }

        // 删除原两个顶点间的连线，然后原两个顶点分别与新生成的这个顶点相连 right here
        string key = PolygonLine.MakeKey( p1.GetGUID(), p2.GetGUID() );
        _polygon.RemoveOneLine(key);

        PolygonPoint new_point = NewPoint();
        new_point.transform.parent = _polygon.transform;
        vecTempPos = ( p1.transform.localPosition + p2.transform.localPosition) / 2.0f;
        vecTempPos.z = 0.0f;
        new_point.transform.localPosition = vecTempPos;

        if ( ( nNewIndex == _polygon.m_lstPoint.Count - 1 ) && nOtherIndex == 0 )
        {
            _polygon.m_lstPoint.Add(new_point);
        }
        else
        {
            _polygon.m_lstPoint.Insert(nNewIndex, new_point);
        }

        ClearCurSelectPoint();

        _polygon.OnPointListChange();

        _polygon.Link2PointsToGenerateLine(p1, new_point);
        _polygon.Link2PointsToGenerateLine(p2, new_point);
        m_eOp = ePolygonEditOperation.none;
    }

    public static PolygonPoint NewPoint()
    {
        GameObject goPoint = GameObject.Instantiate( (GameObject)Resources.Load("MapEditor/Polygon/prePolygonPoint") );
       // GameObject goPoint = GameObject.Instantiate(m_prePolygonPoint);
        PolygonPoint point = goPoint.GetComponent<PolygonPoint>();
        point.SetGUID(s_nPointGUID++ );
        return point;
    }

    public static PolygonLine NewLine()
    {
        GameObject goLine = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/Polygon/prePolygonLine"));
        return goLine.GetComponent<PolygonLine>();
        //return GameObject.Instantiate(s_Instance.m_prePolygonLine).GetComponent<PolygonLine>();
    }

    public static Polygon NewPolygon()
    {
        GameObject goPolygon = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/Polygon/prePolygon"));
        Polygon polygon = goPolygon.GetComponent<Polygon>();// GameObject.Instantiate(s_Instance.m_prePolygon).GetComponent<Polygon>();
        polygon.transform.localPosition = Vector3.zero;
        return polygon;
    }

    void PickOnePoint_RemoveOnePoint( PolygonPoint point )
    {
        if ( _polygon.m_lstPoint.Count <= 3  )
        {
            MsgBox( "一个多边形至少三个顶点，不能再删了!!!!!" );
            return;
        }

        int nIndex = point.GetIndex();
        int nLastCount = _polygon.m_lstPoint.Count - 1;

        // 先删除这个顶点关联的线段
        List<PolygonLine> lst = point.GetRelatedLines();
        for ( int i = lst.Count - 1; i >= 0; i-- )
        {
            PolygonLine line = lst[i];
            if ( line == null )
            {
                continue;
            }
            _polygon.RemoveOneLine( line.GetKey() );
            GameObject.Destroy( line.gameObject);
        }

        // 删除这个顶点
        _polygon.RemovePoint( point );

        PolygonPoint p1 = null;
        PolygonPoint p2 = null;
        int nNewIndex1 = 0;
        int nNewIndex2 = 0;

        if ( nIndex == 0 )
        {
            nNewIndex1 = 0;
            nNewIndex2 = _polygon.m_lstPoint.Count - 1;
        }
        else if ( nIndex == nLastCount)
        {
            nNewIndex1 = 0;
            nNewIndex2 = _polygon.m_lstPoint.Count - 1;
        }
        else
        {
            nNewIndex1 = nIndex - 1;
            nNewIndex2 = nIndex;
        }
        p1 = _polygon.m_lstPoint[nNewIndex1];
        p2 = _polygon.m_lstPoint[nNewIndex2];
        _polygon.Link2PointsToGenerateLine( p1, p2 ); // right here
        m_eOp = ePolygonEditOperation.none;
    }

    void ClearCurSelectPoint()
    {
        if (m_CurSelectedPoint)
        { 
            m_CurSelectedPoint.SetColor(Color.white);
        }
        m_CurSelectedPoint = null;
    }

    public void AddOneLine(string key, PolygonLine line)
    {
        m_dicLine[key] = line;
    }

    public void AddOneLine(int nIndex1, int nIndex2, PolygonLine line)
    {
        string key = nIndex1 < nIndex2 ? nIndex1 + "," + nIndex2 : nIndex2 + "," + nIndex1;
        line.SetKey(key);
        m_dicLine[key] = line;
    }

    void MoveOnePoint()
    {
        if ( m_eOp != ePolygonEditOperation.none )
        {
            return;
        }

        if (m_CurMovingPoint == null)
        {
            return;
        }

        m_CurMovingPoint.transform.localPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void CreateNode(XmlDocument xmlDoc, XmlNode parentNode, string name, string value)
    {
        XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);
        node.InnerText = value;
        parentNode.AppendChild(node);
    }

    void Save()
    {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
            xmlDoc.AppendChild(node);
            XmlElement root = xmlDoc.CreateElement("root");
            xmlDoc.AppendChild(root);

            // 自动生符合形状的碰撞器
            //_polygonCollider.points = new Vector2[_polygon.m_lstPoint.Count];
            Vector2[] ary = new Vector2[_polygon.m_lstPoint.Count];
            Vector2 vec = new Vector2();
            for (int i = 0; i < _polygon.m_lstPoint.Count; i++)
            {
                PolygonPoint point = _polygon.m_lstPoint[i];
                vec.x = point.transform.localPosition.x;
                vec.y = point.transform.localPosition.y;
                ary[i] = vec;

                CreateNode(xmlDoc, root, "P", (point.transform.localPosition.x) + "," + (point.transform.localPosition.y));
            }
            //_polygonCollider.points = ary;

            //保存
			//string szFileName = GetPolygonFileName ( m_szCurSelectedPolygonName );
			//xmlDoc.Save(szFileName);
		StartCoroutine( UploadPolygonXmlFile(  m_szCurSelectedPolygonName, xmlDoc.InnerXml ) );

    }

	public static string GetPolygonFileName( string szPolygonName )
	{
		return AccountManager.c_szYingShe + "Polygon/" + szPolygonName + ".xml";
	}

    public void Load()
    {
        /*
        OpenWinForm.OpenFileDialog op = new OpenWinForm.OpenFileDialog();
        op.Title = "打开一个存档文件";
        string szFileName = CyberTreeMath.ReOrganizeFilePath(Application.streamingAssetsPath);
        szFileName += "MapData\\PolygonElement\\";
        op.InitialDirectory = szFileName;
        
        op.Filter = "XML文件(*.xml)|*.xml";
        if (op.ShowDialog() == OpenWinForm.DialogResult.OK || op.ShowDialog() == OpenWinForm.DialogResult.Yes)
        {
            string selectName = op.FileName;
            Reset();

            string[] aryPointPos = LoadXML(selectName);
            _polygon = NewPolygon();
            _polygon.GeneratePolygon(aryPointPos, true);
        }
        else
        {
            return;
        }
        */
    }


	void LoadPolygon( string szPolygonName )
	{
		ResourceManager.DestroyPolygon ( _polygon );
		// right here 4
		//string[] aryPointPos = LoadXML( PolygonEditor.GetPolygonFileName( szPolygonName ) );
		_polygon = NewPolygon();
		//_polygon.GeneratePolygon(aryPointPos, true);
		StartCoroutine( LoadPolygonByXml( szPolygonName, _polygon, true ) );
	}

	IEnumerator LoadPolygonByXml( string szPolygonName, Polygon polygon, bool bPolygonEditor = false )
	{
		string szFileName = AccountManager.url + "Polygon_" + szPolygonName + ".xml";
		WWW www = new WWW ( szFileName );
		yield return www;
		XmlNode root = null;
		XmlDocument myXmlDoc = StringManager.CreateXmlByText ( www.text, ref root );
		string[] aryPointPos = new string[root.ChildNodes.Count];
		for (int i = 0; i < root.ChildNodes.Count; i++)
		{
			XmlNode node = root.ChildNodes[i];
			aryPointPos[i] = node.InnerText;
		}
		polygon.GeneratePolygon ( aryPointPos );
	}
	// right here 5
    public static string[] LoadXML( string szFileName )
    {
        string[] aryPointPos = null;

        XmlDocument myXmlDoc = new XmlDocument();
        myXmlDoc.Load(szFileName );// "/StreamingAssets/MapData/PolygonElement/polygon_001.xml");
        XmlNode rootNode = myXmlDoc.SelectSingleNode("root");
        aryPointPos = new string[rootNode.ChildNodes.Count];
        for (int i = 0; i < rootNode.ChildNodes.Count; i++)
        {
            XmlNode node = rootNode.ChildNodes[i];
            aryPointPos[i] = node.InnerText;
        }

        return aryPointPos;
    }

	public void CreateNewPolygonOK( string szName )
	{
		if (!StringManager.CheckPolygonNameValid (szName)) {
			return;
		}

		// 新建一个xml文件
		string szFileName = PolygonEditor.GetPolygonFileName( szName );
		XmlDocument xmlDoc = new XmlDocument();
		XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
		xmlDoc.AppendChild(node);
		XmlNode root = xmlDoc.CreateElement("root");
		xmlDoc.AppendChild(root);
		//xmlDoc.Save ( szFileName );
		StartCoroutine( UploadPolygonXmlFile( szName, xmlDoc.InnerXml ) );

		_PolygonList.AddItem( szName );
		_PolygonList.SelectByIndex ( _PolygonList.GetCount() - 1 );

		m_lstPolygons.Add ( szName );

		UpdatePolygonListXmlFile ();

		BeginDrawPath ();
	}

	IEnumerator UploadPolygonXmlFile( string szPolygonName, string szContent ) 
	{
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (  "name",  "Polygon_" + szPolygonName + ".xml"  );  
		wwwForm.AddField (  "content",  szContent );
			wwwForm.AddBinaryData( "content", System.Text.Encoding.Default.GetBytes (szContent ), "Polygon_" + szPolygonName, "xml" );
		WWW www = new WWW(AccountManager.url, wwwForm);
		yield return www;
		Debug.Log ( www.text );  
	}

	void UpdatePolygonListXmlFile()
	{
		_root.RemoveAll();
		for (int i = 0; i < m_lstPolygons.Count; i++)
		{
			IOManager.CreateNode( _XmlDoc, _root, "P", m_lstPolygons[i]);
		}

		StartCoroutine ( UpLoadPolygonListFile(  _XmlDoc.InnerXml) );
	}

	IEnumerator UpLoadPolygonListFile( string szContent)
	{
		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField (  "name",  "PolygonList.xml"  );  
		wwwForm.AddField (  "content",  szContent );
		wwwForm.AddBinaryData( "content", System.Text.Encoding.Default.GetBytes (szContent ), "PolygonList", "xml" );
		WWW www = new WWW(AccountManager.url, wwwForm);
		yield return www;
		Debug.Log ( www.text );  
	}

    public void Exit()
    {
        SceneManager.LoadScene("Scene");
    }

}

