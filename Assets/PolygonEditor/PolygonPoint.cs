﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonPoint : MonoBehaviour {

    public SpriteRenderer _srMain;
    List<PolygonLine> m_lstRelatedLine = new List<PolygonLine>();

    int m_nGUID = 0;
    int m_nIndex = 0;

    public TextMesh _text;

    // Use this for initialization
    void Start () {
		
	}   
	
	// Update is called once per frame
	void Update () {
       
    }

    void OnMouseDown()
    {
        //PolygonEditor.s_Instance.PickPoint( this );
        PolygonEditor.s_Instance.PickOnePoint(this);
    }

    public bool isAlreadyFull()
    {
        return m_lstRelatedLine.Count >= 2;
    }

    public Vector3 GetLocalPosition()
    {
        return this.transform.localPosition;
    }

    public Vector3 GetPosition()
    {
        return this.transform.position;
    }

    public void RelateToLine( PolygonLine line )
    {
        m_lstRelatedLine.Add( line );
    }

    public void UnRelatedToLine()
    {
        for ( int i = m_lstRelatedLine.Count - 1; i >= 0; i-- )
        {
            PolygonLine line = m_lstRelatedLine[i];
            if ( line == null )
            {
                continue;
            }
            GameObject.Destroy( line.gameObject );
        }
        m_lstRelatedLine.Clear();
    }

    public List<PolygonLine> GetRelatedLines()
    {
        return m_lstRelatedLine;
    }

    public void SetIndex( int nIndex )
    {
        m_nIndex = nIndex;
        if (_text)
        {
            _text.text = m_nIndex.ToString();
        }
    }

    public int GetIndex()
    {
        return m_nIndex;
    }

    public void SetColor( Color color )
    {
        _srMain.color = color;
    }

    public void SetGUID( int nGUID )
    {
        m_nGUID = nGUID;
    }

    public int GetGUID()
    {
        return m_nGUID;
    }
}


