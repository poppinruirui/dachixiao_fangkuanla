﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CPlayerIns : MonoBehaviour {

    string m_szAccount = "";
    List<Ball> m_lstBalls = new List<Ball>();
    public Player _Player = null;

    static Vector3 vecTempPos = new Vector3();

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        InitingBalls();

        IntervalLoop_0_3();
        IntervalLoop_1();

        DoingExplode();
    }

    public void SetPlayer(Player player)
    {
        _Player = player;
        if (_Player != null) // 如果遇到“孤儿球”群体，这里_Player就会为null
        {
            _Player.SetBallList(m_lstBalls);
        }
    }

    public Player GetPlayer()
    {
        return _Player;
    }

    public void SetAccount(string szAccount)
    {
        m_szAccount = szAccount;
    }

    public string GetAccount()
    {
        return m_szAccount;
    }

    bool m_bIniting = false;
    int m_nSkinId = 0;
    Color m_Color;
    int m_nColorId = 0;
    string m_szPlayerName = "";
    byte[] m_bytesBallsData = null;
    float m_fBeginInitingTime = 0;
    public void BeginInitBalls(byte[] bytesBallsData, string szAccount, int nSkinId, int nColor, string szPlayerName)
    {
        m_fBeginInitingTime = Main.GetTime();
        m_bytesBallsData = bytesBallsData;
        SetColorId( nColor );
        m_Color = ResourceManager.s_Instance.GetColorByPlayerId(m_nColorId);
        SetSkinId(nSkinId );
        SetAccount(szAccount);
        m_bIniting = true;
        m_nInitingIndex = 0;

        SetPlayerName(szPlayerName);
    }

    public void SetPlayerName( string szPlayerName)
    {
       m_szPlayerName = szPlayerName;
    }

    public string GetPlayerName()
    {
        return m_szPlayerName;
    }

    public void SetColorId( int nColorId )
    {
        m_nColorId = nColorId;
    }

    public int GetColorId()
    {
        return m_nColorId;
    }

    public void SetSkinId( int nSkinId )
    {
        m_nSkinId = nSkinId;
    }

    public int GetSkinId()
    {
        return m_nSkinId;
    }

    const float c_fInitingBallsInterval = 0.1f;
    float m_fInitingBallsTimeCount = 0f;
    int m_nInitingIndex = 0;
    void InitingBalls()
    {
        if (!m_bIniting)
        {
            return;
        }

        // 配置文件初始化成功之前，不要初始化Player。因为网络读取文件是异步的。
        if (!MapEditor.s_Instance.IsMapInitCompleted())
        {
            return;
        }

        if (_Player != null && _Player.IsMainPlayer())
        {

        }
        else
        {
            m_fInitingBallsTimeCount += Time.deltaTime;
            if (m_fInitingBallsTimeCount < c_fInitingBallsInterval)
            {
                return;
            }
            m_fInitingBallsTimeCount = 0;
        }

  

        if ( m_nInitingIndex >= Main.s_Instance.m_fMaxBallNumPerPlayer )
        {
            EndInitBalls();
            return;
        }

       

        // 兼容所有的情况，比如：这群球先是作为“孤儿球”被实例化出来；后来它们的Player上线了，则重新接管它们。甚至，作为孤儿球的身份实例化到一部分的时候，Player突然上线了。都可以兼容，不会出错。
        Ball ball = null;
        if (m_nInitingIndex < m_lstBalls.Count)
        {
            ball = m_lstBalls[m_nInitingIndex];
            ball.SetPlayer(_Player); // ghost ball在这里得到的Player是null。 如果该player后来又上线了，则可以重新接管自己的球球
        }
        else
        {
            ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
            ball.SetIndex(m_nInitingIndex);
            ball.SetPlayer(_Player); // ghost ball在这里得到的Player是null。 如果该player后来又上线了，则可以重新接管自己的球球
            ball.GenerateMeshShape(m_nSkinId, m_Color);
            ball.transform.SetParent(this.transform);
            ball.SetDead(true);
            ball.SetPlayerIns(this);
            m_lstBalls.Add(ball);
        }

        ball.SetPlayerName(m_szPlayerName);


        m_nInitingIndex++;
    }

    // Master-Client负责同步（这里必须做容错，不然网络丢包就麻烦了）[to rongcuo]
    // 容错机制：如果规定时间内没有收到返回包，则再发一次，直到收到为止
    void EndInitBalls()
    {
        m_bIniting = false;
        DoSomeCollisionIgnore();

        float fTimeElapseSinceBegin = Main.GetTime() - m_fBeginInitingTime;

        bool bBallAlreadyExists = false;
        if (m_bytesBallsData != null)
        {
            bBallAlreadyExists = AnalyzeBallData(m_bytesBallsData, fTimeElapseSinceBegin);
        }

        if (_Player)
        {
            _Player.SetPlayerInitCompleted(true);

            if (_Player.IsMainPlayer()) // 本客户端MainPlayer，包括master-client
            {
                if ( !bBallAlreadyExists )
                {
                    _Player.DoReborn();
                }
            }
            else // 其它客户端的player
            {
               
            }

            _Player.SetMyDataLoaded(true);
            _Player.DoSomethingWhenLogin();
        }
        else // 孤儿球（其所属的Player已经掉线）
        {

        }
       
       
    }

    public Ball GetBallByIndex( int nBallIndex )
    {
        return m_lstBalls[nBallIndex];
    }

    // [to youhua] 关于碰撞器的优化还要继续做，做到极致。
    public void DoSomeCollisionIgnore()
    {
        for (int i = 0; i < m_lstBalls.Count - 1; i++)
        {
            Ball ball1 = m_lstBalls[i];


            for (int j = i; j < m_lstBalls.Count; j++)
            {
                Ball ball2 = m_lstBalls[j];

                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._ColliderDust, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._ColliderDust, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball1._ColliderTemp, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Collider, ball2._ColliderTemp);

                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderDust);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._Collider);
                Physics2D.IgnoreCollision(ball1._TriggerMergeSelf, ball2._ColliderTemp);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderDust);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._Collider);
                Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._ColliderTemp);


                Physics2D.IgnoreCollision(ball2._Trigger, ball1._Trigger);
                Physics2D.IgnoreCollision(ball2._Trigger, ball1._TriggerMergeSelf);
                Physics2D.IgnoreCollision(ball1._Trigger, ball2._TriggerMergeSelf);


                if (!IsMainPlayer())
                {
                    Physics2D.IgnoreCollision(ball2._TriggerMergeSelf, ball1._TriggerMergeSelf);
                }
            } // end for j
        } // end for i
    }

    public bool IsOrphan()
    {
        return _Player == null;
    }

    public bool IsMainPlayer()
    {
        if (_Player != null )
        {
            return _Player.IsMainPlayer();
        }

        return false;
    }

    byte[] _bytesLiveBallData = new byte[10240];
    public byte[] GenerateLiveBallData( ref int nBlobSize)
    {
        StringManager.BeginPushData(_bytesLiveBallData);
        StringManager.PushData_Int(0);
        StringManager.PushData_Short( (short)m_nColorId ); // 色号（每个玩家分配到一个专属颜色）
        
        int nLiveBallNum = 0;
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            StringManager.PushData_Short( (short)ball.GetIndex() );
            StringManager.PushData_Float( ball.GetSize() );
            StringManager.PushData_Float(ball.GetPosX());
            StringManager.PushData_Float(ball.GetPosY());
            StringManager.PushData_Float(ball.GetCurShellTime());
            StringManager.PushData_Float(ball.GetTotalShellTime());
            nLiveBallNum++;
        }

        nBlobSize = StringManager.GetBlobSize();
        StringManager.SetCurPointerPos( 0 );
        StringManager.PushData_Int(nLiveBallNum);
       
        if (nLiveBallNum > 0)
        {
            return _bytesLiveBallData.Take(nBlobSize).ToArray();
        }
        else
        {
            return null;
        }
        
    }

    public bool AnalyzeBallData(byte[] bytes, float fTimeElapseSinceBegin)
    {
        StringManager.BeginPopData(bytes);
        int nLiveBallNum = StringManager.PopData_Int();
        if (nLiveBallNum == 0)
        {
            return false;
        }
        /*
        int nLenOfAccount = StringManager.PopData_Short();
        string szAccount = StringManager.PopData_String(nLenOfAccount);
        */
        int nColorId = StringManager.PopData_Short();

        for (int i = 0; i < nLiveBallNum; i++)
        {
            int nBallIndex = StringManager.PopData_Short();
            float fSize = StringManager.PopData_Float();
            float fPosX = StringManager.PopData_Float();
            float fPosY = StringManager.PopData_Float();
            float fCurShellTime = StringManager.PopData_Float();
            float fTotalShellTime = StringManager.PopData_Float();
            Ball ball = m_lstBalls[nBallIndex];
            ball.SetDead(false);
            ball.SetSize(fSize);
            float fRadius = 0f;
            ball.SetVolume(CyberTreeMath.Scale2Volume(fSize, ref fRadius));
            ball.SetRadius(fRadius);
            ball.SetActive(true);
            vecTempPos.x = fPosX;
            vecTempPos.y = fPosY;
            vecTempPos.z = -fSize;

            ball.SetPos(vecTempPos);

            fCurShellTime += fTimeElapseSinceBegin;
            ball.Local_SetShellInfo(fTotalShellTime, fCurShellTime);
            ball.Local_BeginShell();
            ball.UpdateShell();

            if (_Player)
            {
                _Player.SetOneBallUsed(nBallIndex);
            }
        }

        return true;
    }

    public void DestroyBall( int nBallIndex )
    {
        Ball ball = m_lstBalls[nBallIndex];
        ball.SetDead( true );
    }


    // 0.3秒的轮询
    float m_fIntervalLoopTimeElapse_0_3 = 0f;
    void IntervalLoop_0_3()
    {
        m_fIntervalLoopTimeElapse_0_3 += Time.deltaTime;
        if (m_fIntervalLoopTimeElapse_0_3 < 0.3f)
        {
            return;
        }
        m_fIntervalLoopTimeElapse_0_3 = 0f;

        // 更新分球壳的显示
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead() || (!ball.GetActive()))
            {
                continue;
            }

            ball.UpdateShell();
        }
    }

    // 1秒的轮询
    float m_fIntervalLoopTimeElapse_1 = 0f;
    void IntervalLoop_1()
    {
        m_fIntervalLoopTimeElapse_1 += Time.deltaTime;
        if (m_fIntervalLoopTimeElapse_1 < 1f)
        {
            return;
        }
        m_fIntervalLoopTimeElapse_1 = 0f;

        ProcessPlayerName();
    }

    void ProcessPlayerName()
    {
        float fCamSize = CCameraManager.s_Instance.GetCurCameraSize();
        for ( int i = 0; i < m_lstBalls.Count; i++ )
        {
            Ball ball = m_lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }
            if ( !ball.GetActive() )
            {
                continue;
            }
            ball.ProcessPlayerName(fCamSize); // [to youhua]这儿是可以优化的，利用球球尺寸的排序
        }
    }

    // 衰减    
    float m_fAttenuateTimeLapse = 0f;
    public void Attenuate()
    {
        /*
        if ( _Player )
        {
            if ( !_Player.IsMainPlayer() )
            {
                return;
            }
        }
        else
        {

        }

        m_fAttenuateTimeLapse += Time.deltaTime;
        if (m_fAttenuateTimeLapse < 1f)
        {
            return;
        }
        m_fAttenuateTimeLapse = 0f;

        // 只把吃进去的体积拿来衰减。基础体积不参与衰减
        // 基础体积分为三个部分：游戏初始时赋予的 + 道具加成的（貌似目前这种道具废弃了） + 升级时加成的
        float fPlayerEatVolume = GetEatVolume();
        if (fPlayerEatVolume == 0f)
        {
            return;
        }
        int nCurLiveBall = GetCurLiveBallNum();
        if (nCurLiveBall == 0)
        {
            return;
        }
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (ball.IsDead())
            {
                continue;
            }
            ball.DoAttenuate(fPlayerEatVolume);
        }
        */
    }

    public void RemoveOneBall( int nBallIndex )
    {
        if (nBallIndex >= m_lstBalls.Count)
        {
            return;
        }
        Ball ball = m_lstBalls[nBallIndex];
        ball.SetDead( true );
    }

    bool m_bDead = false;
    public bool CheckIfPlayerDead(int nEaterOwnerId = 0)
    {
        for (int i = 0; i < m_lstBalls.Count; i++)
        {
            Ball ball = m_lstBalls[i];
            if (!ball.IsDead())
            { // 只要还有一个球没死，玩家就没死
                m_bDead = false;
                return m_bDead;
            }
        }
        m_bDead = true;

        return m_bDead;
    }

    public bool IsDead()
    {
        return m_bDead;
    }

    public void SetDead( bool bDead )
    {
        m_bDead = bDead;
    }

    ///// 炸球相关


    List<CExplodeNode> m_lstExplodingNode = new List<CExplodeNode>();
    List<CExplodeNode> m_lstRecycledExplodeNode = new List<CExplodeNode>();
    public CExplodeNode GetOneExplodeNode()
    {
        CExplodeNode node;
        if (m_lstRecycledExplodeNode.Count == 0)
        {
            node = GameObject.Instantiate(Main.s_Instance.m_preExplodeNode).GetComponent<CExplodeNode>();
        }
        else
        {
            node = m_lstRecycledExplodeNode[0];
            node.gameObject.SetActive(true);
            node.lstChildBallIndex.Clear();
            m_lstRecycledExplodeNode.RemoveAt(0);
        }

        return node;
    }

    public void RecycleOneExplodeNode(CExplodeNode node)
    {
        node.gameObject.SetActive(false);
        m_lstRecycledExplodeNode.Add(node);
    }

    public void AddOneNodeToExplodingList(CExplodeNode node)
    {
        m_lstExplodingNode.Add(node);
    }

    void DoingExplode()
    {
        for (int i = m_lstExplodingNode.Count - 1; i >= 0; i--)
        {
            CExplodeNode node = m_lstExplodingNode[i];
            bool bEnd = false;
            if (node.IsExploding())
            {
                bEnd = node.DoExplode();
            }
            else
            {
                if (node.IsWaiting())
                {
                    bEnd = node.DoWaiting();
                }
                else
                {
                    bEnd = true;
                }
            }

            if (bEnd)
            {
                m_lstExplodingNode.RemoveAt(i);
                RecycleOneExplodeNode(node);
            }
        }
    }



    ///// end 炸球相关

}
